# Introduction

This guide will give an overview of important information related to the *Vault* container image.

## Getting Started

### How to Use the Container

To run the Vault image, run the following commands:

```shell
docker run -it -p 8200:8200 <IMAGEID>
```

**NOTE:** To run it must have a config file at the path */vault/vaultconfig/vault-config.json*. This is necessary for the helm chart to work properly.

**Additional Debug Information**
If you want to run Vault locally, there are 2 methods. The first is to:

1. Put a `COPY` statement below the one that copies that *entrypoint* file that copies a *vault-config.json* file from the *bin* folder into */vault/config*.
2. Change the `CMD` statement to read `CMD ["server", "--config=/vault/config/vault-config.json"]`
    - This will allow Vault to run fully in memory. This should never be used for production setups.
    - The *vault-config.json* file currently located in the *bin* folder should not be used as is. It needs to have some values configured before being used.
        - `masterURL` can be set to 127.0.0.1 for local testing.
        - `storage` can be changed to a local file for testing.

The second is to:

1. Change the `CMD` statement to read `CMD ["server", "-dev"]`
2. This will allow you to run Vault in *Dev* Server Mode.
    - You can learn more about what can be done with this [here](https://www.vaultproject.io/docs/concepts/dev-server.html).

### Configuration Notes

The following changes must be made before deployment:

- Change the **Build** arg in the dockerfile if a different version is being used.
- Helm chart changes:
  - *tag* & *secret* values in *values.yml* Not currently stored in Helm repo.
  - Add *vaults.secret.yml*. This is not stored in the Helm repo.
  - *vaults.secrets.yml* will need a tls section, containing *hosts*, *cert*, *key*, and *secretname* values.
  - The above are not stored in the repo currently due to security concerns. **(2020,01/09)**
  - Before deploying, it

### Additional Notes

- */vault/logs* is made available to use as a location to store audit logs, if desired; */vault/file* is made available to use as a location with the file storage backend, if desired; the server will be started with */vault/config* as the configuration directory so you can add additional config files in that location.
- The entry point script uses dumb-init as the top-level process to reap any zombie processes created by Vault sub-processes. 
- For production derivatives of this container, you shoud add the *IPC_LOCK* capability so that Vault can mlock memory.
- By default you'll get a single-node development server that stores everything in RAM and bootstraps itself. Don't use this configuration for production.
- There is an option for additional encryption to be added to Vault. To enable it, you will need to:
  - `mkdir -p /vault/cert` in the run section where you create the multiple other directories.
  - `VOLUME /vault/certs` just before you `EXPOSE 8200`.
  - `COPY bin/tls.* /vault/certs/`, which also means creating the cert files to go in the *bin* folder.

## Current Hardening Status

Currently hardened. Read below for more details.

- Uses the *ubi8* image as base. **(01/06/2020)**
- Confirmed running non-root. **(01/09/2020)**
- Passes STIG scan. **(01/09/2020)**
  - *(CCE-27209-6) Verify and Correct File Permissions with RPM* acceptable risk present.
- Passes Twistlock scan. **(01/09/2020)**
- Passes Anchore scan. **(01/09/2020)**

## Locations

*Be sure to include the url for the location if necessary, and any additional notes that may be necessary to access it.*

### Current Deployment Locations

**Unhardened Version**
Deployment ring 3, with highest level achieved IL4.

**Hardened Version**
Not deployed anywhere at this time.

## Additional Documentation

### Vault

- [Vault Official Site](https://www.vaultproject.io/)
- [Vault 1.3.1](https://www.hashicorp.com/blog/vault-1-3/)
- [Hashicorp's Vault Courses](https://learn.hashicorp.com/vault)

#### Installation

- Installation - <https://www.vaultproject.io/docs/install/index.html>
- Commands - <https://www.vaultproject.io/docs/commands/index.html>
- Configuration - <https://www.vaultproject.io/docs/configuration/index.html>