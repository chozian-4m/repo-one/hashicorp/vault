ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ubi8
ARG BASE_TAG=8.5

FROM vault:1.10.1 AS source

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY --from=source /bin/vault /bin/vault
COPY scripts/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

RUN groupadd -g 1001 vault && \
    useradd -r -u 1001 -m -s /sbin/nologin -g vault vault && \
    mkdir -p /vault/logs && \
    mkdir -p /vault/file && \
    mkdir -p /vault/config && \
    chown -R vault:vault /vault && \
    chmod 755 /usr/local/bin/docker-entrypoint.sh

EXPOSE 8200
USER vault

HEALTHCHECK --interval=5m --timeout=30s --start-period=1m --retries=3 \
  CMD curl -f http://locahost:8200/v1/sys/health?standbyok=true || exit 1

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["server"]
